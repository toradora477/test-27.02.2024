# Технічне завдання для розробника

Створіть веб-сторінку з формою для користувача. В якості результатів виконаної роботи надати URL на сторінку зі створеною форму.

## Форма повинна містити наступні поля:
- Ім'я
- Email
- Введення тексту

## Додаткові критерії для форми:
- Поле email повинне бути обов’язковим для заповнення. Ім'я та поле для тексту повинні бути не обов'язковими для заповнення.
- Якщо дані мають помилки/форма заповнена невірно - форма повинна проінформувати користувача про некоректність заповнення.
- Якщо форма заповнена та заповнена вірно - на Email 6weeks.12h@gmail.com повинен бути відправлений лист з інформацією про заявку. Лист повинен бути наступного формату:
  - Тема листа "6weeks - Форма заповнена"
  - Тіло листа повинне мати перелік даних, залишених в формі.

## Зовнішній вигляд форми:
На смак розробника. Але підсумковий зовнішній вигляд у тому числі є фактором оцінки тестового завдання.
